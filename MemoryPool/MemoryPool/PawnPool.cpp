#include "PawnPool.h"
#include "Pawn.h"

Pawn* PawnPool::CreatePawn()
{
	// 풀이 비어있지 않은지를 확인한다.
	if (firstAvailable == nullptr) {
		// full memory pool, can't create object
		return nullptr;
	}

	// 얻은 객체를 빈칸 목록에서 제거한다.
	Pawn* newPawn = firstAvailable;
	firstAvailable = newPawn->GetNext();
	newPawn->Init(); //inUse true & Init Info
	return newPawn;
}

void PawnPool::DeletePawn(Pawn* deleter)
{
	deleter->Release(); //inUse false
	// 삭제할 객체를 빈칸 리스트 앞에 추가한다.
	deleter->SetNext(firstAvailable);    
	firstAvailable = deleter;
}

PawnPool::PawnPool() : firstAvailable(&pawns[0])
{
	// 모든 객체는 다음 객체를  가리킨다.
	for (int i = 0; i < POOL_SIZE-1; ++i) {
		pawns[i].SetNext(&pawns[i + 1]);
	}

	// 마지막 객체에서 리스트를 종료한다.
	pawns[POOL_SIZE - 1].SetNext(nullptr);
}

PawnPool::~PawnPool()
{
	firstAvailable = nullptr;
	for (int i = 0; i < POOL_SIZE; ++i) {
		pawns[i].Release();
	}
}


