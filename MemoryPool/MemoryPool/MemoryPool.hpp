#ifndef MEMORY_POOL_H
#define MEMORY_POOL_H

// 내부단편화를 막기위한 메모리풀
template <typename T, const size_t BlockSize = 4096>
class MemoryPool{
public:
    /* Member types */
    typedef T           value_type;
    typedef T*          pointer;
    typedef T&          reference;
    typedef const T*    const_pointer;
    typedef const T&    const_reference;
    typedef size_t      size_type;

public:
    /* Member functions */
    //template <class U, class... Args> void construct(U* p, Args&&... args);
    //template <class U> void destroy(U* p);

	template <class...Args> pointer newElement(Args&&... args);
    void deleteElement(pointer p);

    //size_t max_size() const noexcept;

private:
    // 가변 템플릿 인자 패키징
    template <class U, class... Args> void construct(U* p, Args&&... args);
    template <class U> void destroy(U* p);

    pointer allocate(size_type n = 1, const_pointer hint = 0);
    void    deallocate(pointer p, size_type n = 1);

public:
    MemoryPool() noexcept;
    MemoryPool(const MemoryPool& memoryPool) = delete;
    MemoryPool(MemoryPool&& memoryPool) = delete;

    ~MemoryPool() noexcept;

    MemoryPool& operator=(const MemoryPool& memoryPool) = delete;
    MemoryPool& operator=(MemoryPool&& memoryPool) = delete;

private:
    union Slot {
        value_type element;
        Slot* next;
        // isUse
    };

    typedef Slot    slot_type;
    typedef Slot*   slot_pointer;

    slot_pointer currentBlock;
    slot_pointer currentSlot;
    slot_pointer lastSlot;
    slot_pointer freeSlots;
};


template<typename T, size_t BlockSize>
inline MemoryPool<T, BlockSize>::MemoryPool()
noexcept
{
    currentBlock_ = nullptr;
    currentSlot_ = nullptr;
    lastSlot_ = nullptr;
    freeSlots_ = nullptr;
}

template <typename T, size_t BlockSize>
inline MemoryPool<T, BlockSize>::~MemoryPool()
noexcept
{
    //slot_pointer_ curr = currentBlock_;
    //while (curr != nullptr) {
    //    slot_pointer_ prev = curr->next;
    //    operator delete(reinterpret_cast<void*>(curr));
    //    curr = prev;
    //}
}

template<typename T, const size_t BlockSize>
template<class ...Args>
inline typename MemoryPool<T, BlockSize>::pointer
MemoryPool<T, BlockSize>::newElement(Args&&... args)
{
    pointer result = allocate();
    construct<value_type>(result, std::forward<Args>(args)...);
    return result;
}

template <typename T, const size_t BlockSize>
inline void
MemoryPool<T, BlockSize>::deleteElement(pointer p)
{
    if (p != nullptr) {
        p->~value_type(); // 소멸자 호출?
        deallocate(p);
    }
}

#endif // !MEMORY_POOL_H