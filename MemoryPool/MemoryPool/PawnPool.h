#pragma once
#include "Pawn.h"

// 오브젝트 풀링 기법 : 메모리 풀과는 다름
class PawnPool {
public:
	Pawn*	CreatePawn();
	void	DeletePawn(Pawn* deleter);
	// copy
	// 

public:
	explicit PawnPool();
	PawnPool(const PawnPool& rhs) = delete;
	PawnPool(PawnPool&& rhs) = delete;

	~PawnPool();

	PawnPool& operator=(const PawnPool& rhs) = delete;
	PawnPool& operator=(PawnPool&& rhs) = delete;

private:
	static const size_t POOL_SIZE = 5;
	Pawn pawns[POOL_SIZE];
	Pawn* firstAvailable;

	// inuse
	// next 
};



