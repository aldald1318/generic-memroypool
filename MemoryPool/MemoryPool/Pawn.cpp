#include "Pawn.h"

bool Pawn::Init()
{
	inUse = true;
	state.live.posX = 0;
	state.live.posY = 0;
	state.live.posZ = 0;

	return true;
}

bool Pawn::Release()
{
	inUse = false;

	return true;
}

Pawn::Pawn() : inUse(false)
{
	state.next = nullptr;
	state.live.posX = 0;
	state.live.posY = 0;
	state.live.posZ = 0;

}

//Pawn::Pawn(const Pawn& rhs)
//{
//}
//
//Pawn::Pawn(Pawn&& rhs) noexcept
//{
//}

Pawn::~Pawn()
{
}

//Pawn& Pawn::operator=(const Pawn& rhs)
//{
//	// TODO: 여기에 return 문을 삽입합니다.
//	return *this;
//}
//
//Pawn& Pawn::operator=(Pawn&& rhs) noexcept
//{
//	// TODO: 여기에 return 문을 삽입합니다.
//	return *this;
//}
