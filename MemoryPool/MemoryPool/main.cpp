#include <iostream>
#include <Windows.h>
using namespace std;


#include "PawnPool.h"
#include "Pawn.h"

#include "MemoryPool.hpp"

class T {

};

int main()
{
	MemoryPool<T> a;

	PawnPool pp;

	Pawn* p1 = pp.CreatePawn();
	Pawn* p2 = pp.CreatePawn();

	cout << p1->GetPosX() << endl;
	pp.DeletePawn(p1);
	pp.DeletePawn(p2);

	Pawn* p3 = pp.CreatePawn();
	Pawn* p4 = pp.CreatePawn();
	pp.DeletePawn(p3);
	pp.DeletePawn(p4);

	system("pause");
}