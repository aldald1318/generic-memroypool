#pragma once

class Pawn{
	friend class PawnPool;
	bool inUse;

public:
	double	GetPosX() const { return state.live.posX; }

protected:
	bool	Init();
	bool	Release();

	Pawn*	GetNext() const { return state.next; }
	void	SetNext(Pawn* const _next) { state.next = _next; }

protected:
	explicit Pawn();
	//explicit Pawn(const Pawn& rhs);
	//explicit Pawn(Pawn&& rhs) noexcept;

	virtual ~Pawn();

	//Pawn& operator=(const Pawn& rhs);
	//Pawn& operator=(Pawn&& rhs) noexcept;

protected:
	union {
		// 사용 중일 때의 상태
		struct {
			double posX, posY, posZ;
		} live;

		// 사용 중이 아닐  때의 상태
		Pawn* next;
	}state;
};

